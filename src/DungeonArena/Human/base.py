# This is the code for your base. Decide which unit to build each frame.
# Units you build will go into the self.built array.
# Destroy the enemy base within 60 seconds!
# Check out the Guide at the top for more info.

# CHOOSE YOUR HERO! You can only build one hero.
hero = 'tharin'
#hero = 'tharin'  # A fierce knight with battlecry abilities.
#hero = 'hushbaum'  # A fiery spellcaster hero.

if (hero and not self.builtHero):
    self.builtHero = self.build(hero)
    return


# Soldiers are hard-to-kill, low damage melee units with 2s build cooldown.
# Archers are fragile but deadly ranged units with 2.5s build cooldown.
buildOrder = ['soldier', 'soldier', 'soldier', 'archer', 'archer']
type = buildOrder[self.built.length % buildOrder.length]
#self.say('Unit #' + self.built.length + ' will be a ' + type)
self.build(type)