enemies = self.getEnemies()
nearestEnemy = self.getNearest(enemies)
if not nearestEnemy: return
nearAnEnemy = self.distance(nearestEnemy) < 4


if nearAnEnemy:
    #self.shield()
    self.attack(nearestEnemy)
else:
    self.move(nearestEnemy.pos)