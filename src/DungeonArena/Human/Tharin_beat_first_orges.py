# This code is used for codecombat - Dungeon Arena, Human sides hero:Tharin code
# This code beats the first place ogres player

# Tharin is a melee fighter with shield, warcry, and terrify skills.
# self.shield() lets him take one-third damage while defending.
# self.warcry() gives allies within 10m 30% haste for 5s, every 10s.
# self.terrify() sends foes within 30m fleeing for 5s, once per match.

friends = self.getFriends()
enemies = self.getEnemies()
if enemies.length is 0:
    return  # Chill if all enemies are dead.
enemy = self.getNearest(enemies)
friend = self.getNearest(friends)
eq = 0
for ee in enemies :
    if ee.type == 'base' :
        enemy = ee
	if self.distance(ee) <10 :
		eq = eq+1
# Which one do you do at any given time? Only the last called action happens.
#if not self.getCooldown("warcry"): self.warCry()
if eq > 0 and not self.getCooldown("terrify"): 
    self.terrify()
#self.shield()
else :
    self.attack(enemy)

# You can also command your troops with self.say():
#self.say("Defend!", "targetPos": {"x": 60, "y": 30})
self.say("Attack!", {"target": enemy})
#self.say("Move!", "targetPos": {"x": 50, "y": 40})

# You can store state on self across frames:
#self.lastHealth = self.health
