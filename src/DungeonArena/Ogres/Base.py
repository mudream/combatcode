# This is the code for your base. Decide which unit to build each frame.
# Units you build will go into the self.built array.
# Destroy the enemy base within 60 seconds!
# Check out the Guide at the top for more info.

# Choose your hero! You can only build one hero.
#hero = None
hero = 'ironjaw'  # A leaping juggernaut hero, type 'brawler'.
#hero = 'yugargen'  # A devious spellcaster hero, type 'shaman'.
if (hero and not self.builtHero):
    self.builtHero = self.build(hero)
    return


# Munchkins are weak melee units with 1.25s build cooldown.
# Throwers are fragile, deadly ranged units with 2.5s build cooldown.
buildOrder = ['munchkin', 'munchkin', 'munchkin', 'thrower']
type = buildOrder[self.built.length % buildOrder.length]
#self.say('Unit #' + self.built.length + ' will be a ' + type)
#self.build(type)
self.build('thrower')