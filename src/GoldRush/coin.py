
emeny = self.getEnemies()[0]

if emeny.gold >= 140 and self.flag == None :
    self.terrify()
    self.flag = True
    return

items = self.getItems()

if(items[0]):
    mypos = self.pos
    
    move_x = 0.0;
    move_y = 0.0;

    for i in items :
        coin_x = i.pos.x
        coin_y = i.pos.y
        gold = i.bountyGold

        now_dis = self.distance(i)

        s = gold / (now_dis * now_dis * now_dis)

        vec_x = coin_x - mypos.x;
        vec_y = coin_y - mypos.y;

        vec_x *= s
        vec_y *= s

        move_x += vec_x
        move_y += vec_y 
        
    l = (move_x * move_x + move_y * move_y)**0.5

    move_x /= l;
    move_y /= l;

    final = Vector(mypos.x + move_x, mypos.y + move_y, mypos.z)

    self.move(final)

