# Tharin is a melee fighter with shield, warcry, and terrify skills.
# self.shield() lets him take one-third damage while defending.
# self.warcry() gives allies within 15m 40% haste for 5s, every 10s.
# self.powerUp() makes his next strike do 5x damage.
# self.terrify() sends foes within 30m fleeing for 5s, once per match.

friends = self.getFriends()
enemies = self.getEnemies()
if enemies.length is 0:
    return  # Chill if all enemies are dead.
friend = self.getNearest(friends)

# Which one do you do at any given time? Only the last called action happens.

#if self.health <= 40:
#    return

if len(friends) >= 5:
    if not self.getCooldown("warcry"):
        self.warcry()
        return


# if self.health <= 40:
#    count_ok = 0
#    for enemy in enemies:
#        if self.distance(enemy) <= 30:
#            count_ok += 1
#    if count_ok >= 5 and not self.getCooldown("terrify"):
#        self.terrify()
#        return

if not self.getCooldown("power-up") and not self.hasEffect('power-up'):
    self.powerUp()
    return

enemy = self.getNearest(enemies)
if self.distance(enemy) >= 10 :
    self.attack(enemy)
    return

self.shield()

# attack
#enemy = self.getNearest(enemies)
#self.attack(enemy)

# You can store state on self across frames:
self.lastHealth = self.health
