# This is your commander's code. Decide which unit to build each frame.
# Destroy the enemy base within 90 seconds!
# Check out the Guide at the top for more info.

#### 1. Choose your hero. ###########################################
# Heroes cost 100 gold. You start with 100 and earn 10 per second.

if len(self.built) == 0:
    hero = 'tharin'  # A fierce knight with battlecry abilities, type 'knight'.
    #hero = 'hushbaum'  # A fiery spellcaster hero, type 'librarian'.
    #hero = 'anya';  # A stealthy ranged attacker, type 'captain'.
    if 100 <= self.gold:
        self.build(hero)
    return

#### 2. Choose which unit to build each turn. #######################
# Soldiers are hard-to-kill, low damage melee units who cost 20 gold.
# Archers are fragile but deadly ranged units who cost 25 gold.
# Units you build wilo into the self.built list.
buildOrder_after = ['soldier', 'soldier', 'soldier', 'soldier', 'soldier', 'archer', 'archer']
buildOrder = ['soldier', 'soldier', 'archer', 'archer', 'archer']

buildOrder = ['soldier', 'soldier', 'soldier', 'soldier', 'archer']

if len(self.built) <= 30000:
    buildType = buildOrder[len(self.built) % len(buildOrder)]
else:
    buildType = buildOrder_after[len(self.built) % len(buildOrder)]


if self.buildables[buildType].goldCost <= self.gold:
    self.build(buildType)

####### 3. Command minions to implement your tactics. ################
# Minions obey 'move' and 'attack' commands.
# Click on a minion to see its API.

minions = self.getFriends()

enemy_range = self.getByType('thrower')
enemy_foot = self.getByType('munchkin')
hero_fang = self.getByType('fangrider')
hero_sham = self.getByType('shaman')
hero_braw = self.getByType('brawler')


for minion in minions:
    if self.commandableTypes.indexOf(minion.type) == -1:  
        continue 
    if minion.type == 'soldier' and len(hero_fang)>0 :
        self.command(minion, 'attack', minion.getNearest(hero_fang))
    else if minion.type == 'soldier' and len(enemy_range)>0 :
        self.command(minion, 'attack', minion.getNearest(enemy_range))
    else if len(hero_fang)>0 :
        self.command(minion, 'attack', minion.getNearest(hero_fang))
    else if len(enemy_range) > 0  :
        self.command(minion, 'attack', minion.getNearest(enemy_range))
    else:
        self.command(minion, 'attack', minion.getNearestEnemy())
