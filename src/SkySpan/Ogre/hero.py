# The Brawler is a huge melee hero with mighty mass.
# self.throw() hurls an enemy behind him.
# self.jumpTo() leaps to a target within 20m every 10s.
# self.cleave() mightily attacks everyone in front of the Brawler.
# self.stomp() knocks everyone away, once per match.

friends = self.getFriends()
enemies = self.getEnemies()
if enemies.length is 0:
    return  # Chill if all enemies are dead.
enemy = self.getNearest(enemies)
friend = self.getNearest(friends)
self.attack(enemy)

# Which one do you do at any given time? Only the last called action happens.
#if not self.getCooldown("jump"): self.jumpTo(enemy.pos)
#if not self.getCooldown("stomp") and self.distance(enemy) < 10: self.stomp()
#if not self.getCooldown("cleave"): self.cleave(enemy)
#if not self.getCooldown("throw"): self.throw(enemy)
#self.attack(enemy)

# You can store state on self across frames:
#self.lastHealth = self.health
