# This is your commander's code. Decide which unit to build each frame.
# Destroy the enemy base within 90 seconds!
# Check out the Guide at the top for more info.

friends = self.getFriends()
enemies = self.getEnemies()

#### 1. Choose your hero. ###########################################
# Heroes cost 100 gold. You start with 100 and earn 10 per second.

hero = 'ironjaw'
#hero = 'ironjaw'  # A leaping juggernaut hero, type 'brawler'.
#hero = 'yugargen'  # A devious spellcaster hero, type 'shaman'.
#hero = 'dreek'  # A deadly spear hero, type 'fangrider'.
if (hero and not self.builtHero):
    self.builtHero = self.build(hero)
    return

#### 2. Choose which unit to build each turn. #######################
# Munchkins are weak melee units who cost 11 gold.
# Throwers are fragile, deadly ranged units who cost 25 gold.
# Units you build will go into the self.built list.

buildOrder = ['munchkin', 'munchkin', 'munchkin', 'munchkin', 'thrower', 'thrower', 'thrower', 'thrower', 'munchkin', 'munchkin', 'munchkin', 'munchkin', 'thrower', 'thrower']
buildType = buildOrder[len(self.built) % len(buildOrder)]
if self.buildables[buildType].goldCost <= self.gold:
    self.build(buildType)

####### 3. Command minions to implement your tactics. ################
# Minions obey 'move' and 'attack' commands.
# Click on a minion to see its API.

for friend in friends:
    if self.commandableTypes.indexOf(friend.type) == -1:  # TODO: fix Python in
    #if not (minion.type in self.commandableTypes):
        continue  # You can't command heroes.
    #self.command(minion, 'move', {"x": 12, "y": 30})
    self.command(friend, 'attack', self.getNearest(enemies))
